#!/usr/bin/env python3
import discord
import time
import random
from datetime import datetime, date

global client
client = discord.Client()
@client.event
async def on_message(message):
	commands = {"typerace": typeRace, "rps": rPs, "hangman": hangMan, "blackjack": blackJack}
	if message.content.startswith('!') and message.content[1:] in commands:
		await commands[message.content[1:]](message)

@client.event
async def on_ready():
	print("ready")
	await client.change_presence(game=discord.Game(name="Normies get out"))

#pulls random message from discord server and waits for user to enter the same sentence and calcluates wpm
async def typeRace(message):
	type_test = await randomMessage()
	time_start = int(time.localtime()[4]*60+time.localtime()[5])
	await client.send_message(message.channel, type_test)
	response = await client.wait_for_message(timeout=60.0, author=message.author)
	time_end = int(time.localtime()[4]*60+time.localtime()[5])
	how_long_it_took = int(time_end-time_start)
	if response is None:
		await client.send_message(message.channel, "too slow loser")
		return
	else:
		error_check = type_test.split(' ')
		response_check = response.content.split(' ')
		count = 0
		for correct, user_input in zip(error_check, response_check):
			if correct == user_input:
				count+=1
		wpm = int((count*60)/how_long_it_took)
		accuracy = int(float(count/len(response_check))*100)
		if wpm > 75:
			reaction = ":fire:"
		elif wpm > 35:
			reaction = ":eggplant:"
		else:
			reaction = ":snail:"
		final_score = "WMP: {} accuracy: {}% {} ".format(str(wpm), str(accuracy), reaction)
		await client.send_message(message.channel, final_score)

#waits 1 seconds for user to choose rock paper or scissors and decides who wins
async def rPs(message):
	choices = ["rock", "paper", "scissors"]
	what_wins = {"rock": "paper", "paper":"scissors", "scissors":"rock"}
	bot_choice = random.choice(choices)
	await client.send_message(message.channel, "1")
	time.sleep(0.5)
	await client.send_message(message.channel, "2")
	time.sleep(0.5)
	await client.send_message(message.channel, "shoot")
	player_choice = await client.wait_for_message(timeout=1, author=message.author)
	await client.send_message(message.channel, bot_choice)
	if player_choice is None:
		final = "You take too long I win :sunglasses: "
	elif player_choice.content == bot_choice:
		final = "It\'s a tie :thinking: "
	elif what_wins[bot_choice] == player_choice.content:
		final = "{} beats {} you win :weary: ".format(player_choice.content, bot_choice)
	else:
		final = "{} beats {} I win :sunglasses: ".format(bot_choice, player_choice.content)
	await client.send_message(message.channel, final)

#waits 10 second and checks logs for anyone who says join thne loops muliplayer function until the game is won or lost
async def hangMan(message):
	await client.send_message(message.channel, "starting game of hangman in 10 seconds !join to join")
	utc_time_now = datetime.utcnow()
	time.sleep(10)
	players = await logCheck(utc_time_now, message.channel, "!join")
	await client.send_message(message.channel, "Starting game of hangman with {}".format(" ,".join(players.values())))
	current_game = Hangman(message.channel, players, await randomMessage())
	await client.send_message(message.channel, " ".join(current_game.sentence_list))
	while current_game.game_lost == False and current_game.game_won == False and len(current_game.players) > 0:
			await muliplayerManager(current_game)
	if current_game.game_won:
		await client.send_message(current_game.channel, "congrats you won")
	else:
		await client.send_message(current_game.channel, "wow you lost you suck")
		await client.send_message(current_game.channel, current_game.sentence)

#pulls random setence from discord server and creates a list for that contains - for each letter
class Hangman:
	def __init__(self, channel, players, sentence):
		self.hangman_pics = ["""
+-+
|   |
|
|
|
======
""", """
++
|  |
| O
|
|
======
""", """
++
|  |
| O
|  |
|
======
""", """
++
|  |
| O
|/|
|
======
""", """
++
|  |
| O
|/|\\
|
======""", """
++
|  |
| O
|/|\\
|/
======
""", """
++
|  |
| O
|/|\\
|/ \\
======
"""]
		self.channel = channel
		self.players = players
		self.guesses = []
		self.wrong = 0
		self.game_won = False
		self.game_lost = False
		self.sentence = sentence.lower()
		self.sentence_list = []
		for x in self.sentence:
			if x == " ":
				self.sentence_list.append(x)
			else:
				self.sentence_list.append("-")

#takes a input from the person whose turn it is and checks to see if that letter is in the sentence
	async def manager(self, player):
		player_guess = await client.wait_for_message(timeout=30.0, channel=self.channel, author=player)
		final = None
		if player_guess is None or player_guess.content == "!leave":
			return
		elif len(player_guess.content) > 1:
			if player_guess.content == self.sentence:
				self.game_won = True
				return
			else:
				self.wrong+=1
				final = str(self.hangman_pics[self.wrong])
		elif player_guess.content in self.guesses:
			final = "That was already guessed dumbo"
		else:
			self.guesses.append(player_guess.content)
			if player_guess.content in self.sentence:
				indices = [i for i, x in enumerate(self.sentence) if x == player_guess.content]
				for x in indices:
					self.sentence_list[x] = player_guess.content
				final = " ".join(self.sentence_list)
			else:
				self.wrong+=1
				final = str(self.hangman_pics[self.wrong])
				self.game_lost = True if self.wrong == 6 else False
		await client.send_message(self.channel, final)
		if "".join(self.sentence_list) == self.sentence:
			self.game_won = True

async def blackJack(message):
	await client.send_message(message.channel, "starting game of blackjack in 10 seconds !join to join")
	utc_time_now = datetime.utcnow()
	time.sleep(10)
	players = await logCheck(utc_time_now, message.channel, "!join")
	current_game = Blackjack(message.channel, players)
	while len(current_game.players) > 0:
		quitters={}
		output = "starting round of blackjack in 30 seconds place bets"
		for player in current_game.players:
			output+="\n{}'s life savings: {}".format(
			current_game.players[player], current_game.players_class[player].life_savings)
		await client.send_message(current_game.channel, output)
		utc_time_now = datetime.utcnow()
		time.sleep(30)
		bets = await logCheck(utc_time_now, current_game.channel, "!bet", content="int")
		for player in current_game.players:
			if not player in bets:
				quitters[player] = current_game.players[player]
			else:
				current_player = current_game.players_class[player]
				current_player.bet = bets[player]
				for _ in range(2):
					current_player.hand.append(random.choice(current_game.deck))
		current_game.dealerDraw()
		sum_hand = current_game.dealer_hand[0]
		hand_output = str(current_game.dealer_hand[0])
		await client.send_message(current_game.channel, "The dealer drew {}".format(hand_output))
		await muliplayerManager(current_game, utc_time_now=utc_time_now, quitters=quitters)
		while sum(current_game.dealer_hand) < 21:
			await client.send_message(
			current_game.channel, "dealer's hand is {}".format(
			" ".join(str(x) for x in current_game.dealer_hand)))
			current_game.dealerDraw()
			sum_hand = sum(current_game.dealer_hand)
			hand_output = " ".join(str(x) for x in current_game.dealer_hand)
			if sum_hand == 21:
				await client.send_message(
					current_game.channel, "dealer's hand is {} which is 21 hahaha".format(hand_output))
				break
			if 11 in current_game.dealer_hand and sum_hand > 21:
				current_game.dealer_hand[current_game.dealer_hand.index(11)] = 1
			elif sum_hand > 21:
				await client.send_message(
				current_game.channel, "dealer's hand is {} which is above 21 congrats".format(hand_output))
				break
			elif sum_hand > 16:
				await client.send_message(
				current_game.channel, "dealer's hand is {} and is standing".format(hand_output))
				break
		for player in players:
			player_hand = sum(current_game.players_class[player].hand)
			if sum_hand > 21 and player_hand > 21:
				pass
			elif player_hand < sum_hand or player_hand > 21:
				current_game.players_class[player].life_savings-=current_game.players_class[player].bet
			elif player_hand > sum_hand:
				current_game.players_class[player].life_savings+=current_game.players_class[player].bet
			current_game.players_class[player].hand = []
		current_game.dealer_hand = []
		current_game.players.update(await logCheck(utc_time_now, current_game.channel, "!join"))
		current_game.updatePlayers()
	await client.send_message(current_game.channel, "Thanks for the money losers")

class Blackjack:
	def __init__(self, channel, players):
		self.game_lost = False
		self.game_won = False
		self.channel = channel
		self.players = players
		self.players_class = {}
		for player in self.players:
			self.players_class[player] = self.Player()
		self.deck = []
		for x in range(2, 12):
			number_of_cards = 16 if x == 10 else 4
			for _ in range(number_of_cards):
				self.deck.append(x)
		self.dealer_hand = []

	def updatePlayers(self):
		for player in self.players:
			if not player in self.players_class:
				self.players_class[player] = self.Player()

	def dealerDraw(self):
		self.dealer_hand.append(random.choice(self.deck))

	async def manager(self, player):
		current_player = self.players_class[player]
		hand = current_player.hand
		hand_output = " ".join(str(x) for x in hand)
		def check(msg):
			return msg.content.startswith('!')
		if sum(hand) == 21:
			await client.send_message(
			self.channel,
			"your bet is {} your hand is {} which is 21 congrats".format(
			current_player.bet, hand_output))
			return
		end = False
		while 1:
			final = None
			await client.send_message(self.channel,
			"your bet is {} your hand is {} !hit, !doubledown, or !stand".format(
			current_player.bet, hand_output))
			response = await client.wait_for_message(timeout=60.0, channel=self.channel, author=player, check=check)
			if response is None or response.content == "!leave":
				return
			if response.content == "!doubledown":
				current_player.bet = current_player.bet*2
			if response.content == "!hit" or response.content == "!doubledown":
				new_card = random.choice(self.deck)
				await client.send_message(self.channel, "you drew a {}".format(new_card))
				hand.append(new_card)
				hand_output = " ".join(str(x) for x in hand)
				if 11 in hand and sum(hand) > 21:
					hand[hand.index(11)] = 1
				if sum(hand) == 21:
					final ="your bet is {} your hand is {} which is 21 congrats".format(
					current_player.bet, hand_output)
					end = True
				elif sum(hand) > 21:
					final = "your hand is {} which is above 21 congrats on losing {}".format(
          hand_output, current_player.bet)
					end = True
			elif response.content == "!stand":
				return
			if final:
				await client.send_message(self.channel, final)
			if end:
				break
			

	class Player:
		def __init__(self):
			self.life_savings = 69420
			self.bet = 0
			self.hand = []

#loops through each player and does the class.manager while checking to see if people leave or join
async def muliplayerManager(current_game, utc_time_now=datetime.utcnow(), late_joiners={}, quitters = {}):
	for player in current_game.players.keys():
		if player in quitters.keys():
			continue
		if current_game.game_lost or current_game.game_won or len(current_game.players) == 0:
			break
		await client.send_message(current_game.channel, "{}'s turn".format(current_game.players[player]))
		await current_game.manager(player)
		quitters.update(await logCheck(utc_time_now, current_game.channel, "!leave"))
		late_joiners.update(await logCheck(utc_time_now, current_game.channel, "!join"))
	for quitter in quitters.keys():
		if quitter in current_game.players:
			del current_game.players[quitter]
	current_game.players.update(late_joiners)

#checks the log for a certain keyword after a certain time and if content=True it returns the contents 
async def logCheck(time_start, channel, keyword, content=False):
	people = {}
	async for log in client.logs_from(channel, after=time_start):
		if log.content.startswith(keyword):
			if log.author.nick in people.keys() or log.author.name in people.keys():
				pass
			elif content != False:
				try:
					people[log.author] = eval(content)(log.content.replace(keyword , ""))
				except:
					continue
			elif log.author.nick is None:
				people[log.author] = log.author.name
			else:
				people[log.author] = log.author.nick
	return people

#pulls random message from discord excluding some servers
async def randomMessage():
	global previous_random
	text_channels = []
	filter_channels = ["mario", "music_bot-spam", "fun_bot-spam", "rules", "resources"]
	for x in client.get_all_channels():
		if str(x.type) == "text" and not x.name in filter_channels:
			text_channels.append(x)
	rand_message = None
	special_number = random.randint(1, 100)
	while 1:
		startday = datetime(2017, 10, 1).toordinal()
		endday = datetime.utcnow().toordinal()
		randomday = date.fromordinal(random.randint(startday, endday))
		specific_channel = random.choice(text_channels)	
		async for log in client.logs_from(specific_channel, limit=200, before=randomday):
			if not log.content.startswith("!") and not log.content.startswith("@") and len(str(log.content)) > 5 and not "http" in str(log.content) and log.author != client.user and not log.content in previous_random and random.randint(1, 100) == special_number:
				rand_message = log.content
				previous_random.append(log.content)
			if not rand_message is None:
				return str(rand_message)

global previous_random
previous_random = []
client.run("Mzg5NTU4Njg4ODA2ODYyODYw.DQ9f3Q.xHEe44Xyw2huhFQxlipgwG0Z9C0")
